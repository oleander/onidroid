# OniDroid
### Onionshare on Android

***

This script will install Onionshare in your termux (android) and let you share files securely
<https://onionshare.org/>

***

## Installation
```
pkg update
pkg install git
git clone https://github.com/oleanderxoxo/onidroid
cd onidroid
bash setup.sh
```

### Usage
```
onionshare /location/of/file.extension
example:
onionshare /sdcard/oleander.jpg
```
